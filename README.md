# Docker ELK 

Основан на официльных образах:

* [elasticsearch](https://registry.hub.docker.com/_/elasticsearch/)
* [logstash](https://registry.hub.docker.com/_/logstash/)
* [kibana](https://registry.hub.docker.com/_/kibana/)


# Требования

## Установка

1. Установить [Docker](http://docker.io).
2. Установить [Docker-compose](http://docs.docker.com/compose/install/) **version >= 1.6**.
3. Клонировать текущий репозиторий

## Увеличить размер доступной памяти max_map_count на хост машине 


```bash
$ sudo sysctl -w vm.max_map_count=262144
```

# Первый запуск

Запустите ELK с помощью *docker-compose*:

```bash
$ docker-compose up -d -f docker-compose.yml
```

Для доступа к интефейсу Kibana UI перейдите по ссылке [http://host:5601](http://host:5601).

# Безопасность. Изменение паролей

После запуска необходимо изменить пароли поумолчанию. Для этого необходимо перейти в раздел managment и изменить данные.
Для того, чтобы передать новые учетные данные в kibana и logstash, необходимо:
создать файл docker-compose.override.yml:
```YML
 version: '2'
 
 services:
 logstash:
  volumes:
      - ./logstash/config.local:/etc/logstash/conf.d
 kibana:
    volumes:
      - ./kibana/config.local/:/etc/kibana/
```

создать папки с локальными конфигами:
./kibana/config.local/kibana.yml
установить elasticsearch.username и elasticsearch.password
./logstash/config.local/logstash.yml
установить новые user и password


Запустите ELK с помощью *docker-compose* (без указания файла -f docker-compose.yml):

```bash
$ docker-compose up -d
```
